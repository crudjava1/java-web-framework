package ws.annotation;

import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;
/**
 * Annotation pour les classes qui doivent être traitées comme un Controller
 * Tous les controllers doivent avoir un attribut de type <strong>RequestData</strong>
 * @author Bary Rakotoarivelo
 * @author Marcus Hunald
 */
@Documented
@Retention(RUNTIME)
@Target(TYPE)
public @interface Controller {

}
