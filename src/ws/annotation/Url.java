package ws.annotation;

import static java.lang.annotation.ElementType.*;
import static java.lang.annotation.RetentionPolicy.RUNTIME;
import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;
/**
 * Annotation pour associé une URL à une classe ou une méthode  
 * @author Bary Rakotoarivelo
 * @author Marcus Hunald
 */
@Documented
@Retention(RUNTIME)
@Target(value={TYPE, METHOD})
public @interface Url {
	/**
	 * URL à associé (ex: /user)
	 * @return .
	 */
	String value();
	/**
	 * Verb autorisé pour accéder à l'URL (ex: POST, GET, PUT, DELETE)
	 * @return .
	 */
	String method() default "ALL";
}