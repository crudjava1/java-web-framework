package ws.annotation;

import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

/**
 * Spécifie le rôle autorisé pour accéder à l'URL 
 * @author Bary Rakotoarivelo
 * @author Marcus Hunald
 */

@Documented
@Retention(RUNTIME)
@Target({ TYPE, METHOD })
public @interface Authorized {
	/**
	 * Nom de la variable de session (ex: Role, Profil)
	 * @return .
	 */
	String name();
	/**
	 * Valeur (ex: root)
	 * @return .
	 */
	String value();
}
