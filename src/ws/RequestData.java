package ws;

import java.util.Enumeration;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
/**
 * Classe qui permet de stocker les variables de la requête (HttpServletRequest) qui pourraient être utilisées dans la Controller
 * @author Bary Rakotoarivelo
 * @author Marcus Hunald
 */
public class RequestData {
	
	public static HashMap<String, Object> data = new HashMap<String, Object>();
	public static HashMap<String, Object> session = new HashMap<String, Object>();

	public RequestData() {}

	/**
	 * Initialiser la classe RequestData avec l'objet request en argument
	 * @param request
	 */
	public static void init(HttpServletRequest request) {
		RequestData.data.clear();
		RequestData.session.clear();
		RequestData.setRequestData(request);
		RequestData.setSessionAttributes(request.getSession());
	}
	
	/**
	 * Stocke les variables de la requête avec leur valeur correspondante
	 * @param req
	 */
	public static void setRequestData(HttpServletRequest req) {
		Enumeration<String> enume = req.getParameterNames();
		String key = null;
		while (enume.hasMoreElements()) {
			key = (String) enume.nextElement();
			RequestData.data.put(key, req.getParameter(key));
		}
		enume = req.getAttributeNames();
		while (enume.hasMoreElements()) {
			key = (String) enume.nextElement();
			RequestData.data.put(key, req.getAttribute(key));
		}
	}

	/**
	 * Stocke les variables de session avec leur valeur correspondante
	 * @param session
	 */
	public static void setSessionAttributes(HttpSession session) {
		Enumeration<String> enume = session.getAttributeNames();
		String key = null;
		while (enume.hasMoreElements()) {
			key = (String) enume.nextElement();
			RequestData.session.put(key, session.getAttribute(key));
		}
	}

	/**
	 * Ajouter (key, value) dans la session actuelle
	 * @param key
	 * @param value
	 */
	public static void put(String key, Object value) {
		session.put(key, value);
	}
	
	/**
	 * Vider la session actuelle
	 */
	public static void clear() {
		session.clear();
	}
	
	/** 
	 * Supprimer une variable de session
	 * @param key
	 */
	public static void remove(String key) {
		session.remove(key);
	}
	
	/**
	 * Vérifie l'existence d'une variable dans la session actuelle
	 * @param key
	 * @return booléen
	 */
	public static boolean containsKey(String key) {
		return session.containsKey(key);
	}
	
	/**
	 * Récupère la valeur d'une variable de session
	 * @param key
	 * @return booléen
	 */
	public static String getSession(String key) {
		return  (session.get(key) != null) ? session.get(key).toString() : "";
	}
	
	public static String getData(String key) {
		return  (data.get(key) != null) ? data.get(key).toString() : "";
	}
}
