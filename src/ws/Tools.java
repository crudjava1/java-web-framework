package ws;

import java.io.File;
import java.lang.reflect.Method;
import java.lang.reflect.Parameter;
import java.net.URL;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Enumeration;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import ws.annotation.Authorized;
import ws.annotation.Controller;
import ws.annotation.Url;
import ws.exception.UrlException;
/**
 * Classe utilitaires
 * @author Bary Rakotoarivelo
 * @author Marcus Hunald
 */
public class Tools {
	public static Response invoke(Method method, Class<?> classe, HttpServletRequest request) throws Exception {
		if (method.getParameterCount() == 0) { /** method has no arguments */
			return (Response) method.invoke(classe.newInstance());
		}
		Parameter[] parameters = method.getParameters();
		Object[] arguments = new Object[parameters.length];
		Class<?> type = null;
		for (int i = 0; i < parameters.length; i++) { /** set each method parameter */
			type = parameters[i].getType();
			if (Tools.isPrimitive(type)) {
				throw new Exception("Method cannot have a primitive parameter");
			} else {
				arguments[i] = Tools.setData(type, request);
			}
		}
		return (Response) method.invoke(classe.newInstance(), arguments);
	}

	private static <T> T setData(Class<T> classe, HttpServletRequest request) throws Exception {
		Method[] methods = classe.getMethods();
		T result = null;
		result = classe.newInstance();
		Object paramValue = null;
		for (int i = 0; i < methods.length; i++) {
			paramValue = null;
			paramValue = request.getParameter(Tools.getFieldName(methods[i].getName()));
			if (paramValue != null && !paramValue.toString().isEmpty() && methods[i].getName().startsWith("set")) {
				methods[i].invoke(result, Tools.cast(methods[i].getParameterTypes()[0], paramValue.toString()));
			}
		}
		return result;
	}

	private static boolean isPrimitive(Class<?> classe) {
		String className = classe.getSimpleName().toLowerCase();
		List<String> primitive = new ArrayList<String>();
		Collections.addAll(primitive, new String[] { "int", "float", "string", "double", "timestamp" });
		return primitive.contains(className);
	}

	@SuppressWarnings("unchecked")
	public static <T> T cast(Class<T> type, String value) throws Exception {
		try {
			String typeStr = type.getSimpleName().toLowerCase();
			switch (typeStr) {
			case "int":
				return (T) Integer.valueOf(value);
			case "float":
				return (T) Float.valueOf(value);
			case "string":
				return (T) value;
			case "double":
				return (T) Double.valueOf(value);
			case "timestamp":
				return (T) Timestamp.valueOf(value);
			default:
				throw new Exception("Type not supported :" + typeStr);
			}
		} catch (Exception e) {
			throw new Exception("Invalid cast");
		}
	}

	private static String getFieldName(String setter) {
		String name = setter.substring(3);
		return name.substring(0, 1).toLowerCase() + name.substring(1);
	}

	/**
	 * Get Method in the given class associated with the request URL
	 * 
	 * @param classe     class that has the method we are looking for
	 * @param requestUrl request URL
	 * @param request    HttpServletRequest for checking http VERB and SESSION
	 *                   information
	 * @return Method
	 * @throws Exception
	 */
	public static Method getMethod(Class<?> classe, String[] requestUrl, HttpServletRequest request) throws Exception {
		Method[] methods = classe.getMethods();
		Url url = null;
		String methUrl = null;
		/**
		 * No method is specified so we call the method associated with --index-- URL
		 */
		if (requestUrl[0].length() == 0) {
			requestUrl[0] = "index";
		}
		for (int i = 0; i < methods.length; i++) {
			url = methods[i].getAnnotation(Url.class);
			methUrl = (url != null && !url.value().isEmpty()) ? url.value() : methods[i].getName();
			if (requestUrl[0].equals(methUrl)) {
				/** Url verification */
				if (url == null) {
					return methods[i];
				}
				String method = url.method();
				if (method.compareToIgnoreCase(request.getMethod()) == 0
						|| method.compareToIgnoreCase("all") == 0) { /** verb verification */
					Authorized auth = methods[i].getAnnotation(Authorized.class);
					if (auth == null) {
						return methods[i];
					}
					System.out.println("Session:" + String.valueOf(request.getSession().getAttribute(auth.name())));
					String role = String.valueOf(request.getSession().getAttribute(auth.name()));
					if (role.toLowerCase().equals(auth.value().toLowerCase())) { /** auth verification */
						return methods[i];
					} else {
						throw new Exception("Access denied");
					}
				}
			}
		}
		throw new UrlException("No method associated with URL");
	}

	/**
	 * Get class associated with the given request URL
	 * 
	 * @param requestUrl request URL
	 * @return Class
	 * @throws Exception
	 */
	public static Class<?> getClasse(String[] requestUrl) throws Exception {
		Url url = null;
		Controller ctrl = null;
		Class<?> classe = null;
		/** get all classes in package */
		Class<?>[] classes = Tools.getClasses("controller");
		String classUrl = null;
		for (int i = 0; i < classes.length; i++) {
			/** Check Controller annotation */
			ctrl = classes[i].getAnnotation(Controller.class);
			if (ctrl == null) {
				continue;
			}
			url = classes[i].getAnnotation(Url.class);
			classUrl = (url != null) ? url.value() : classes[i].getSimpleName();
			if (requestUrl[0].startsWith(classUrl)) {
				// remove class url so that we can retrieve method URL easily
				requestUrl[0] = requestUrl[0].replace(classUrl, "");
				if (requestUrl[0].length() != 0 && !requestUrl[0].startsWith("/")) {
					throw new Exception("invalid Url format");
				}
				requestUrl[0] = requestUrl[0].replaceAll("^/", "");
				classe = classes[i];
				break;
			}
		}
		// class not found
		if (classe == null) {
			throw new UrlException("Corresponding class not found");
		}
		return classe;
	}

	/**
	 * Get request URL in array with checking not supported URL
	 * 
	 * @param requestUrl resuest URL
	 * @return Class
	 * @throws Exception
	 */
	public static String getUrl(String requestUrl) throws Exception {
		// Remove project name (/servletFramework/path/to/url.extension -> path/to/url.extension)
		requestUrl = requestUrl.replaceAll("^/\\w+/", "");
		// Remove extension at the end of url (path/to/url.extension -> path/to/url)
		requestUrl = requestUrl.replaceAll("\\.\\w+$", "");
		return requestUrl;
	}

	/**
	 * Get all classes in given directoty and package name
	 * 
	 * @param directory   The directory where you are going to do the research
	 * @param packageName The package name where you are going to do the research
	 * @return List
	 * @throws Exception
	 */
	private static List<Class<?>> getRessourceClasses(File directory, String packageName) throws Exception {
		List<Class<?>> classes = new ArrayList<Class<?>>();
		if (!directory.exists()) {
			return classes;
		}
		File[] files = directory.listFiles();
		for (File file : files) {
			if (file.isDirectory()) {
				classes.addAll(Tools.getRessourceClasses(file, packageName + "." + file.getName()));
			} else if (file.getName().endsWith(".class")) {
				classes.add(Class.forName(packageName + '.' + file.getName().split("\\.")[0]));
			}
		}
		return classes;
	}

	/**
	 * Get all classes in given package
	 * 
	 * @param packageName name of package to find classes
	 * @return Class
	 * @throws Exception
	 */
	public static Class<?>[] getClasses(String packageName) throws Exception {
		ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
		Enumeration<URL> resources = classLoader.getResources(packageName.replace('.', '/'));
		ArrayList<Class<?>> classes = new ArrayList<Class<?>>();
		while (resources.hasMoreElements()) {
			classes.addAll(Tools.getRessourceClasses(new File(resources.nextElement().getFile()), packageName));
		}
		return classes.toArray(new Class[0]);
	}
}
