package ws;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
/**
 * Gère les données à retourner au client 
 * @author Bary Rakotoarivelo
 * @author Marcus Hunald
 */
public class Response {
	/**
	 * Status de la reponse
	 */
	private String status;
	/**
	 * Liste de données à retourner
	 */
	private HashMap<String, Object> data = new HashMap<String, Object>();
	/**
	 * Messages de la reponse
	 */
	private HashMap<String, String> message = new HashMap<String, String>();
	/**
	 * Vue à afficher
	 */
	private String vue = "";

	/**
	 * Valeur constantes pour les etats et les types à retourner (ex: json, ...)
	 */
	public static final String JSON = "json"; 
	public static final String ERR = "400";
	public static final String SUCCESS = "200";
	
	/**
	 * Ajout des données dans RequestAttribut  
	 * @param key
	 * @param object
	 */
	public void putData(String key, Object object) {
		this.data.put(key, object);
	}
	
	/**
	 * Ajout des messages à afficher 
	 * @param key
	 * @param val
	 */
	public void putMessage(String key, String val) {
		this.message.put(key, val);
	}
	
	/**
	 * Idem putMessage()
	 * @param status
	 * @param key
	 * @param val
	 */
	public void putMessage(String status, String key, String val) {
		this.status = status;
		this.message.put(key, val);
	}
	
	
	public boolean isJson() {
		return this.vue.compareTo("json") == 0;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public HashMap<String, String> getMessage() {
		return message;
	}

	public void setMessage(HashMap<String, String> msgs) {
		this.message = msgs;
	}
	
	public String getVue() {
		return vue;
	}

	public void setVue(String vue) {
		this.vue = vue;
	}
	
	public Response() {}
	
	public Response(String status) {
		this.status = status;
	}
	
	public Response(String status, HashMap<String, Object> data) {
		this.status = status;
		this.data = data;
	}
	
	public Response(String status, String vue) {
		this.status = status;
		this.vue = vue;
	}
	
	public Response(String status, String vue, HashMap<String, String> msgs) {
		this.status = status;
		this.vue = vue;
		this.message = msgs;
	}
	
	public Response(String status, String vue, String key, Object val) {
		this.status = status;
		this.vue = vue;
		this.putData(key, val);
	}
	
	public void setRequestAttribut(HttpServletRequest request) {
		for (Map.Entry<String, Object> entry : this.data.entrySet()) {
			request.setAttribute(entry.getKey(), entry.getValue());
	    }
		request.setAttribute("message", this.message);
	}
}