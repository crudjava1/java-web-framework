package ws;

import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Filtre des URL en tenant compte de l'extension mise dans Web.xml 
 * @author Bary Rakotoarivelo
 * @author Marcus Hunald
 */
public class RequestFilter implements Filter {
	private String extension = "";
	public void init(FilterConfig filterConfig) {
		this.extension = filterConfig.getInitParameter("ext");
	}
	
	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		HttpServletRequest req = null;
		HttpServletResponse resp = null;
		/*BufferedReader reader = null;
		StringBuffer sb = null;
		String line = null;*/
		try {
			req = (HttpServletRequest)request;
			resp = (HttpServletResponse)response;
			/*reader = request.getReader();
			sb = new StringBuffer();
			while ((line = reader.readLine()) != null)
				sb.append(line);
			req.setAttribute("reqBody", sb.toString());*/
			addCorsHeader(resp);
			if(req.getMethod().compareToIgnoreCase("OPTIONS") == 0) {
				resp.setStatus(HttpServletResponse.SC_ACCEPTED);
				return;
			}
			if(req.getServletPath().endsWith("."+this.extension)) {
				req.setAttribute("url", req.getRequestURI());
				RequestDispatcher r = req.getRequestDispatcher("/mainServlet");
				r.forward(request, response);
			} else {
				chain.doFilter(request, response);
			}	
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private void addCorsHeader(HttpServletResponse response){
        response.addHeader("Access-Control-Allow-Origin", "*");
        response.addHeader("Access-Control-Allow-Methods", "POST, GET, OPTIONS, PUT, DELETE, HEAD");
        response.addHeader("Access-Control-Allow-Headers", "X-PINGOTHER, Origin, X-Requested-With, Content-Type, Accept");
        response.addHeader("Access-Control-Max-Age", "1728000");
    }
}
