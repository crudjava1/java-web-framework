package ws.exception;
/**
 * Gère les erreurs d'URL
 * @author Bary Rakotoarivelo
 * @author Marcus Hunald
 */
@SuppressWarnings("serial")
public class UrlException extends Exception {
	public UrlException(String msg) {
		super(msg);
	}
}
