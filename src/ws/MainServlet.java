package ws;

import java.io.IOException;
import java.io.PrintWriter;
import java.lang.reflect.Method;
import java.util.Enumeration;
import java.util.Map;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.google.gson.Gson;

import ws.exception.UrlException;
/**
 * Servlet principal pour traiter les URL qui se terminent par l'extension spécifiée dans Web.xml
 * @author Bary Rakotoarivelo
 * @author Marcus Hunald
 */
@SuppressWarnings("serial")
@WebServlet("/mainServlet")
public class MainServlet extends HttpServlet {
	public void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/plain");
		RequestDispatcher dispatcher = null;
		try {
			PrintWriter out = null;
			Response resp = null;
			Method method = null;
			/** init(set) requestData value with Session */
			RequestData.init(request);
			/** requestUrl type is String[] to have a pointer on URL */
			String[] requestUrl = new String[] {Tools.getUrl(String.valueOf(request.getAttribute("url")))};
			/** get class and method associated with request URL */
			Class<?> classe = Tools.getClasse(requestUrl);
			method = Tools.getMethod(classe, requestUrl, request);
			/** invoke method */
			resp = Tools.invoke(method, classe, request);
			/** refresh session */
			this.setHttpSession(request);
			if (resp == null) { /** user can't return null response */
				throw new UrlException("Response null in called method");
			}
			if (resp.isJson()) { /** check if service must return JSON */
				out = response.getWriter();
				Gson gson = new Gson();
				out.print(gson.toJson(resp));
			} else { /** service return view (jsp, servlet, other method with [extension]) */
				//request.setAttribute("data", resp.getData());
				resp.setRequestAttribut(request);
				dispatcher = request.getRequestDispatcher(resp.getVue());
				dispatcher.forward(request, response);
			}
		} catch (UrlException ue) {
			ue.printStackTrace();
			dispatcher = request.getRequestDispatcher("/notFound");
			dispatcher.forward(request, response);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Actualiser , mettre à jour les variables de session
	 * @param request
	 */
	public void setHttpSession(HttpServletRequest request) {
		HttpSession httpSession = request.getSession();
		Enumeration<String> enume = httpSession.getAttributeNames();
		/** Remove all session attribute */
		while (enume.hasMoreElements())
			httpSession.removeAttribute((String) enume.nextElement());
		/** Set request session to RequestData.session */
		for (Map.Entry<String, Object> session : RequestData.session.entrySet())
			httpSession.setAttribute((String) session.getKey(), session.getValue());
	}
}
