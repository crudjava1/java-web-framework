package controller;

import java.util.ArrayList;
import java.util.List;

import models.User;
import ws.RequestData;
import ws.Response;
import ws.annotation.Authorized;
import ws.annotation.Controller;
import ws.annotation.Url;

@Url("user")
@Controller
public class UserCtrl {
	
	/*Exemple Begin*/
	static List<User> userList = new ArrayList<User>();
	/*Exemple End*/
	/*
	@Url(value="all", method="get")
	public Response getAll() {
		User user = new User("Ben","Lee", 20);
		RequestData.session.put("nom", user.getNom());
		System.out.println("1 - session: " + RequestData.session.get("nom"));
		return new Response(new User(RequestData.data.get("nom").toString()) ,"index.jsp");
	}
	@Authorized(name="role", value="docteur")
	@Url(value="a/b/c", method="get")
	public Response index() {
		System.out.println("2 - session: " + RequestData.session.get("nom"));
		return new ResponseJson(new User("Ben", "Damm", 17));
	}
	
	@Url(value="setSess", method="post")
	public Response setSessNom() {
		RequestData.session.put("nom", RequestData.data.get("nom"));
		System.out.println("3 - session: " + RequestData.session.get("nom"));
		return new Response(new User(RequestData.data.get("nom").toString()), "index.jsp");
	}
	
	@Url(value="getSess", method="get")
	public Response getSessNom() {
		System.out.println("4 - session: " + RequestData.session.get("nom"));
		if(RequestData.session.get("nom") == null)
			return new Response(null ,"index.jsp", "Session vide");
		return new Response(new User(RequestData.session.get("nom").toString()) ,"index.jsp");
	}
	*/
	@Url(value="connection", method="post")
	public Response connection(User user) {
		RequestData.session.put("nom", user.getNom());
		RequestData.session.put("prenom", user.getPrenom());
		RequestData.session.put("role", user.getRole());
		System.out.println("Session:" + RequestData.session.get("role"));
		return new Response(Response.SUCCESS, "home.jsp", "user", user);
	}
	
	@Url(value="checkSession", method="post")
	public Response checkSession() {
		Response resp = new Response(Response.SUCCESS, Response.JSON);
		resp.setStatus(Response.ERR);
		if(RequestData.session.containsKey("role")) {
			resp.putData("role", new User(RequestData.session.get("nom").toString()));
		}
		return resp;
	}
	
	@Url(value="disconnect", method="post")
	public Response disconnect() {
		RequestData.session.clear();
		return new Response(Response.SUCCESS, "index.jsp");
	}
	
	@Url(value="getAll", method="get")
	public Response getAll() {
		return new Response(Response.SUCCESS, Response.JSON, "users", UserCtrl.userList.toArray(new User[UserCtrl.userList.size()]));
	}
	
	@Authorized(name="role", value="docteur")
	@Url(value="addUser", method="post")
	public Response addUser(User user) {
		System.out.println("Session:" + RequestData.session.get("role"));
		UserCtrl.userList.add(user);
		return new Response(Response.SUCCESS, Response.JSON, "users", UserCtrl.userList.toArray(new User[UserCtrl.userList.size()]));
	}
}
