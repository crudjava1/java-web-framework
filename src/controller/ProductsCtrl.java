package controller;

import models.User;
import ws.annotation.Controller;
import ws.annotation.Url;

@Controller
public class ProductsCtrl {
	public void index(User user) {
		System.out.println("Index function");
		System.out.println(user.toString());
	}
	
	public void getAll() {
		System.out.println("Get all in products");
	}
	
	@Url(value="t")
	public void t() {
		System.out.println("You are in T method");
	}
}
