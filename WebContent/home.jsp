<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<title>Servlet framework</title>
		<style>
			table {
			  font-family: arial, sans-serif;
			  border-collapse: collapse;
			  width: 100%;
			}
			
			td, th {
			  border: 1px solid #dddddd;
			  text-align: left;
			  padding: 8px;
			}
			
			tr:nth-child(even) {
			  background-color: #dddddd;
			}
		</style>
	</head>
	<body>
		<h2 id="user"></h2>
		<form action="/servletFramework/user/disconnect.a" method="post">
			<button type="submit" >Se deconnecter</button>
		</form>
		<br>
		<h2>Ajout utilisateur</h2>
		<form id="add-btn" action="/servletFramework/user/addUser.a" method="post">
			<label>Nom</label>
			<br>
			<input name="nom" type="text" />
			<br>
			<label>Prenoms</label>
			<br>
			<input name="prenom" type="text" />
			<br>
			<label>Role</label>
			<br>
			<select name="role" value="docteur">
				<option value="docteur">Docteur</option>
				<option value="autre">Autre</option>
			</select>
			<br>
			<input type="submit" value="Ajouter" />
		</form>
		<h2>Liste utilisateur</h2>
		<button id="list-btn">Get liste ustilisateur</button>
		<br>
		<table id="list-user">
			<tr>
				<th>Nom</th>
				<th>Prenoms</th>
				<th>Role</th>
			</tr>
		</table>
		<script type="text/javascript" src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
		<script type="text/javascript">
			function addList(data) {
				console.log(data);
				$('tr').remove('.row');
				let users = JSON.parse(data).data;
				users.forEach(user => {
					$(`#list-user`).append(
						`<tr class="row">
							<td>` + user.nom + `</td>
	           				<td>` + user.prenom + `</td>
	           				<td>` + user.role + `</td>
           				</tr>`
           			);
				});
			}
			
			$(document).ready(function() {
				$.ajax({
                    type: "post",
                    url: "/servletFramework/user/checkSession.a",
                    success: function(data){
                    	let resp = JSON.parse(data);
                    	if(resp.status != 200) {
                    		window.location.href="/servletFramework";
                    	}else {
                    		$(`#user`).append("Welcome, " + resp.data.nom);
                    	}
                    }
                });
				
				let form = $(`#add-btn`);
				form.submit(function(e) {
					e.preventDefault();
					$.ajax({
						type: form.attr('method'),
						url: form.attr('action'),
						data: form.serialize(),
	                    success: function(data){
	                    	addList(data);
	                    }
	                });
				});
				
				$(`#list-btn`).click(function() {					
					$.ajax({
	                    type: "get",
	                    url: "/servletFramework/user/getAll.a",
	                    success: function(data){
	                    	addList(data);
	                    }
	                });
				});
			});
		</script>
	</body>
</html>