<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Servlet framework</title>
</head>
<body>
	<div><h1>Connexion</h1></div>
	<form action="/servletFramework/user/connection.a" method="post">
		<label>Nom</label>
		<br>
		<input name="nom" type="text" />
		<br>
		<label>Prenoms</label>
		<br>
		<input name="prenom" type="text" />
		<br>
		<label>Role</label>
		<br>
		<select name="role" value="docteur">
			<option value="docteur">Docteur</option>
			<option value="autre">Autre</option>
		</select>
		<br>
		<input type="submit" value="Se connecter" />
	</form>
</body>
</html>